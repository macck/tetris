package tetris.base;

import console.engine.IConsoleGFXPlayfield;

public class GameEngine {

    private IConsoleGFXPlayfield playfield;
    private TetrominoBlocks blocks;
    private int nPlayfieldWidth, nPlayfieldHeight;
    private String playerName;
    private int score;
    boolean gameEnded;

    public GameEngine(String playerName, IConsoleGFXPlayfield playfield, TetrominoBlocks blocks){
        this.playerName = playerName;
        this.score = 0;
        this.gameEnded = false;
        this.playfield = playfield;
        this.blocks = blocks;
        this.nPlayfieldWidth = playfield.getFieldWidth();
        this.nPlayfieldHeight = playfield.getFieldHeight();

        playfield.createPlayingField(12,18);// according to original tetris dim's
        blocks.CreateAssets();

    }

    /*
    Posx, posy -> location of left top corner of tetromino block within playingField array
 */
    public boolean doesBlockFit(int tetrominoBlockId, int tetrominoRotation, int posx, int posy) {
        for(int px = 0; px < 4; px++) {
            for(int py = 0; py < 4; py++){
                // Get position index transformation for piece
                int indexBlockPiece = blocks.Rotate(px,py, tetrominoRotation);
                // Find the same index within playing field array -> but playfield already does that, just offset
               // int indexFieldPiece = (posy + py) * nPlayfieldWidth + (posx + px);

                if( posx + px >= 0 && posx + px < nPlayfieldWidth) {
                    if(posy + py >=0 && posy + py < nPlayfieldHeight) {
                        // check "global" position of piece in tetromino block piece against plaing field position for collisions
                        if( blocks.getTetromino(tetrominoBlockId).charAt(indexBlockPiece) == 'X'
                                && playfield.getFieldCharOnPos(posx + px, posy + py) != 0){
                            // Warning: getFieldCharOnPos returns character mapped into ABCDEFG=# symbolset
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }


}

package tetris.base;

import console.engine.IConsoleGFXPlayfield;

public class PlayingField implements IConsoleGFXPlayfield {

    private int nFieldWidth;
    private int nFieldHeight;
    private char[] pField;
    private char[] charsMap = {' ','A','B','C','D','E','F','G','=','#'}; // this doesn't belong here, blocks are also mapped etc, maybe gfx feature or gengine?

    private boolean isFieldValueValidCharacter(char fieldValue)
    {
        if( (int) fieldValue <= 9 && (int) fieldValue >= 0){
            return  true;
        }
        else return  false;
    }

    private boolean isFieldWithinBounds(int posx, int posy)
    {
        return (posx < nFieldWidth && posx >= 0 && posy < nFieldHeight && posy >= 0) ? true : false;
    }

    @Override
    public char getFieldCharOnPos(int posx, int posy)
    {
        if(isFieldWithinBounds(posx,posy))
            return charsMap[pField[posy*nFieldWidth + posx]]; // return char representing given code  at pos
        else
            return '0'; // shall i throw exception?
    }

    @Override
    public void setField(int posx, int posy, char fieldValue) {
        if(isFieldWithinBounds(posx,posy))
        {
            if(isFieldValueValidCharacter(fieldValue))
                pField[posy*nFieldWidth+posx] = fieldValue;
        }
    }

    @Override
    public int getFieldHeight() {
        return nFieldHeight;
    }

    @Override
    public int getFieldWidth() {
        return nFieldWidth;
    }

    /*
        Create tetris playing field,
        9 represents the border, 0 represents empty space
     */
    @Override
    public void createPlayingField(int nFieldWidth, int nFieldHeight) {
        if(nFieldHeight > 0 && nFieldWidth > 0) {
            this.nFieldHeight = nFieldHeight;
            this.nFieldWidth = nFieldWidth;
            this.pField = new char[this.nFieldWidth * this.nFieldHeight];
            for(int x = 0; x < nFieldWidth; x++){
                for(int y = 0; y < nFieldHeight; y++) {
                    // check if boundary position, insert 9 at boundaries (represented by #)
                    pField[y*nFieldWidth + x] = (char) ((x == 0 || x == nFieldWidth - 1 || y == nFieldHeight -1 ) ? 9 : 0);
                }
            }
        }
    }
}

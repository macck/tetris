package tetris.base;

public class TetrominoBlocks {
    private String[] tetromino = new String[7];

    public  String getTetromino(int blockId) {
        if(blockId <= 6  && blockId >= 0)
            return tetromino[blockId];

        else throw new ArrayIndexOutOfBoundsException("Block with this id does not exist!");
    }

    /*
    This function for given x,y  from original not rotated coordinates retuns index
    in tetromino array for given rotation
     */

    public  int Rotate(int px, int py, int r){
        switch( r % 4){ // 0 degrees 1*90 ... 270 deg
            case 0: return py * 4 + px; // 0 degrees
            case 1: return 12 + py - (px * 4); // 90 degrees
            case 2: return 15 - px - (py * 4); // 180
            case 3: return 3 + py *  (px + 4);
        }
        return 0;
    }

    public void  CreateAssets(){
    // Tetris block stored in string, this way for visual clue
        tetromino[0] = ("..X.");
        tetromino[0] = tetromino[0] + ("..X.");
        tetromino[0] = tetromino[0] + ("..X.");
        tetromino[0] = tetromino[0] + ("..X.");

        tetromino[1] = ("..X.");
        tetromino[1] = tetromino[1] + (".XX.");
        tetromino[1] = tetromino[1] + ("..X.");
        tetromino[1] = tetromino[1] + ("....");

        tetromino[2] = (".XX.");
        tetromino[2] = tetromino[2] + (".XX.");
        tetromino[2] = tetromino[2] + ("..X.");
        tetromino[2] = tetromino[2] + ("....");

        tetromino[3]  = ("....");
        tetromino[3] = tetromino[3] + (".XX.");
        tetromino[3] = tetromino[3] +(".XX.");
        tetromino[3] = tetromino[3] +("....");

        tetromino[4] = ("..X.");
        tetromino[4] = tetromino[4] + (".XX.");
        tetromino[4] = tetromino[4] + ("..X.");
        tetromino[4] = tetromino[4] + ("....");

        tetromino[5] = ("....");
        tetromino[5] = tetromino[5] + (".XX.");
        tetromino[5] = tetromino[5] + ("..X.");
        tetromino[5] = tetromino[5] + ("..X.");

        tetromino[6] = ("....");
        tetromino[6] = tetromino[6] + (".XX.");
        tetromino[6] = tetromino[6] + (".X..");
        tetromino[6] = tetromino[6] + (".X..");
    }

}

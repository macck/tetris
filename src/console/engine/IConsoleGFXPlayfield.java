package console.engine;

public interface IConsoleGFXPlayfield {
    // does this interface have any sense within context of GfxEngine? Rather should have interface of engine to set some stuff eventually
    public void createPlayingField(int nFieldWidth, int nFieldHeight);
    public char getFieldCharOnPos(int posx, int posy);
    public void setField(int posx, int posy, char fieldValue);
    public int getFieldHeight();
    public int getFieldWidth();
}

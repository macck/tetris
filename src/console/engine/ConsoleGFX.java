package console.engine;
import java.io.*;

public class ConsoleGFX {
    private int bufferSize;
    private BufferedWriter consoleOut;
    private IConsoleGFXPlayfield playfield;
    private int frameWidth;
    private int frameHeight;
    private int frameSize;

    private char[] screenBuff;

    public ConsoleGFX(IConsoleGFXPlayfield playfield) throws UnsupportedEncodingException {
        this.playfield = playfield;
        this.frameWidth= playfield.getFieldWidth();
        this.frameHeight = playfield.getFieldHeight();
        this.frameSize = frameWidth*frameHeight;
        screenBuff = new char[frameHeight*frameWidth];

        bufferSize = 2*(frameHeight * frameWidth);
        consoleOut = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(FileDescriptor.out),
                    "UTF-8"), bufferSize);
    }

    public void gfxRedraw() throws IOException {
        for(int line = 0; line < frameHeight; line++)
        {
            consoleOut.write(screenBuff,line*frameWidth,frameWidth);
            consoleOut.newLine();
        }
        consoleOut.flush();
    }

    public void gfxLoadFrame(char[] frame)
    {
        this.screenBuff = frame;
    }

    public void fromPlayfieldGetFrame(){
        // ISSUE: I dont like the idea of streaming playfield char by char, need some sort of memcpy with mapping
        for(int y = 0; y < frameHeight; y++)
        {
            for(int x = 0; x < frameWidth; x++)
            {
                gfxLoadCharAtPos(x,y,playfield.getFieldCharOnPos(x,y));
            }
        }
    }

    private void gfxLoadCharAtPos(int posx, int posy, char c) {
        this.screenBuff[posy*frameWidth+posx] = c;
    }
}
